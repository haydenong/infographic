(function () {
    'use strict';

    angular
        .module('infographicapp')
        .factory('accountService', accountService);

    accountService.inject = ['$http', '$q'];

    function accountService($http, $q) {
        var service = {
            userLogin: userLogin,
            userSignup: userSignup
        };

        return service;

        ////////////////
        function userLogin(user) {
            var defer = $q.defer();
            $http.post("/auth/local/login", user)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        }

        function userSignup(user) {
            var defer = $q.defer();
            $http.post("/api/users", user)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        }
    }
})();