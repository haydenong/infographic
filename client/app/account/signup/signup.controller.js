(function () {

    angular.module("infographicapp")
        .controller("SignupCtrl", SignupCtrl)


    SignupCtrl.$inject = ["accountService", "$state"]


    function SignupCtrl(accountService, $state) {
        var vm = this;
        vm.submit = submit;
        vm.status = {};

        /////////////////////////////////////
        function submit() {
            accountService.userSignup(vm.user)
                .then(function (result) {
                    console.log("here");
                    $state.go("home");
                    vm.status.message = "Signup sucessful!";
                    vm.status.code = 202;
                })
                .catch(function (err) {
                    vm.status.message = "An error occurred while signing up";
                    vm.status.code = 400;
                })
        };
    };

}());