'use strict';

angular.module('infographicapp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: '/app/account/login/login.html',
                controller: 'LoginCtrl as ctrl'
            })
            .state('signup', {
                url: '/signup',
                templateUrl: '/app/account/signup/signup.html',
                controller: 'SignupCtrl as ctrl'
            });

    });