(function () {

    angular.module("infographicapp")
        .controller("LoginCtrl", LoginCtrl)


    LoginCtrl.$inject = ["accountService", '$state']


    function LoginCtrl(accountService, $state) {
        var vm = this;
        vm.submit = submit;
        vm.status = {};

        /////////////////////////////////////
        function submit() {
            accountService.userLogin(vm.user)
                .then(function (result) {
                    console.log("results " +  JSON.stringify(result));
                    console.log("results " +  JSON.stringify());
                    $state.go('home');
                    vm.status.message = "Login sucessful!";
                    vm.status.code = 202;
                })
                .catch(function (err) {
                    vm.status.message = "An error occurred while logging in";
                    vm.status.code = 400;
                })
        };
    };

}());