'use strict';

angular.module('infographicapp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'app/main/main.html'
                // controller: 'SignupCtrl as ctrl'
            });
           
    });