(function () {
    'use strict';

    angular
        .module('infographictest')
        .service('apiService', apiService);

    apiService.inject = ['$http'];

    function apiService($http) {
        var service = {
            getProfile: getProfile
        };
        return service;
        ////////////////

        function getProfile($http) {
            var defer = $q.defer();
            $http.get("/api/user/:id", user)
                .then(function (result) {
                    defer.resolve(result);
                })
                .catch(function (err) {
                    defer.reject(err);
                });
            return defer.promise;
        }
    }
})();