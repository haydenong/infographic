(function () {
    angular.module("infographicapp", ['ui.router'])
        .config(function ($urlRouterProvider,  $locationProvider) {
            $urlRouterProvider.otherwise("/login");
            $locationProvider.html5Mode(true);
        });
}());