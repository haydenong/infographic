var express = require("express"),
    path = require("path"),
    cookieParser = require("cookie-parser"),
    bodyParser = require("body-parser"),
    session = require("express-session"),
    mongoose = require("mongoose"),
    mongoStore = require("connect-mongo")(session),
    passport = require("passport");

const CLIENT_FOLDER = path.join(__dirname + "/../../client"),
    BOWER_COMPONENTS = path.join(__dirname + "/../../bower_components");

module.exports = function (app, config) {
    app.use(bodyParser.urlencoded({
        extended: false
    }));
    app.use(bodyParser.json());

    app.use(express.static(CLIENT_FOLDER));
    app.use("/bower_components", express.static(BOWER_COMPONENTS));
    
    app.use(session({
        secret: config.secrets.session,
        resave: true,
        saveUninitialized: true,
        store: new mongoStore({
            mongooseConnection: mongoose.connection
        })
    }));
    app.use(cookieParser());

    app.use(passport.initialize());
    app.use(passport.session());
}