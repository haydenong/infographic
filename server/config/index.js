module.exports = {
    secrets: {
        session: "infographictest-secret"
    },
    db: {
        url: "mongodb://localhost/infographictest"
    }
}