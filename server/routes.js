var path = require("path");

const CLIENT_FOLDER = path.join(__dirname + "/../client"),
    MSG_FOLDER = path.join(CLIENT_FOLDER + "/messages");

module.exports = function (app) {

    // Insert routes below
    // app.use('/api/things', require('./api/thing'));


    app.use('/api/users', require('./api/user'));
    app.use('/api/images', require('./api/image'));

    app.use('/auth', require('./auth'));

    app.all('/*', function (req, res, next) {
        // Just send the index.html for other files to support HTML5Mode
        res.sendFile('index.html', {
            root: CLIENT_FOLDER
        });
    });

    app.get("/status/:code", function (req, res) {
        //console.log("Saved user------", req.user);
        var code = parseInt(req.params.code);
        res.status(code).end();
    });

    app.use(function (req, res) {
        res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
    });

    // Error handler: server error
    app.use(function (err, req, res, next) {
        res.status(501).sendFile(path.join(MSG_FOLDER + "/501.html"));
    });
}