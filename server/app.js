var express = require("express"),
    mongoose = require("mongoose"),
    config = require("./config"),
    app = express();

const NODE_PORT = process.env.PORT || 8080;

mongoose.Promise = global.Promise;
mongoose.connect(config.db.url);

app.use(function (req, res, next) {
    console.info('handling request for: ' + req.url);
    next();
});
require("./config/express")(app, config);
require("./routes")(app);

// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});