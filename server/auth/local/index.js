'use strict';
var express = require('express');
var passport = require('passport');
var router = express.Router();

router.post("/login", passport.authenticate("local", {
    successRedirect: '/status/201',
    failureRedirect: '/'
}));

module.exports = router;