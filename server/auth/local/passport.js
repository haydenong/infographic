var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    bcrypt = require('bcryptjs');

exports.setup = function (User) {

    passport.use(new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
            session: true
        },
        function (req, email, password, done) {
            User.findOne({
                email: email.toLowerCase()
            }, function (err, user) {
                if (err) return done(err);

                if (!user) {
                    return done(null, false, {
                        message: 'This email is not registered.'
                    });
                }
                return done(null, user);
            });
            // User.findOne({
            //     where: {
            //         email: username
            //     }
            // }).then(function (result) {
            //     if (!result) {
            //         return done(null, false);
            //     } else {
            //         if (bcrypt.compareSync(password, result.password)) {
            //             return done(null, result);
            //         } else {
            //             return done(null, false);
            //         }
            //     }
            // }).catch(function (err) {
            //     return done(err, false);
            // });

        }
    ));
};