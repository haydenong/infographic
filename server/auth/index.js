'use strict';

var express = require('express'),
    passport = require('passport'),
    config = require('../config/auth'),
    User = require('../api/user/user.model'),
    app = express(),
    router = express.Router();

require('./local/passport').setup(User);
require('./auth.service')(User, app);

// require('./facebook/passport').setup(User, config);
// require('./google/passport').setup(User, config);


router.use('/local', require('./local'));
// router.use('/auth/facebook', require('./facebook'));
// router.use('/auth/google', require('./google'));

module.exports = router;