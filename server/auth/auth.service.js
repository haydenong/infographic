var passport = require('passport');

module.exports = function (User, app) {
    passport.serializeUser(function (user, done) {
        // console.info(req.session)
        console.info("serializing>> " + user.id);
        done(null, user.id);
    });

    passport.deserializeUser(function (id, done) {
        console.info("deserializing>> " + id);
        User.findById(id, function (err, user) {
            if (err) {
                console.info("deserializing err " + err);
                return done(err);
            }
            console.log("deserializeUser " + user);
            done(null, user);
        });
    });

    // route middleware to make sure a user is logged in
    function isLoggedIn(req, res, next) {

        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();

        // if they aren't redirect them to the login page
        res.redirect('/');
    }
};