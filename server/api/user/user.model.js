'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcryptjs'),
    UserSchema = new Schema({
    name: {
        first: String,
        last: String
    },
    email: {
        type: String,
        lowercase: true
    },
    password: String,
    gender: String,
    date_of_birth: Date,
    profile_pic_url: String,
    facebook: {
        id: String,
        token: String
    },
    google: {
        id: String,
        token: String
    },
    created_at: Date,
    updated_at: Date
});

// generating a hash
UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
UserSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('User', UserSchema);