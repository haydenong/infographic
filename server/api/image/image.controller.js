/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /users              ->  index
 * POST    /users              ->  create
 * GET     /users/:id          ->  show
 * PUT     /users/:id          ->  update
 * DELETE  /users/:id          ->  destroy
 */



'use strict';

var _ = require('lodash');
var Image = require('./image.model');

// Get list of images
exports.index = function (req, res) {
    Image.find({
        user_id: req.user.id
    }, function (err, images) {
        if (err) {
            return handleError(res, err);
        }
        return res.status(200).json(images);
    });
};

// Get a single image
exports.show = function (req, res) {
    Image.findById(req.params.id, function (err, image) {
        if (err) {
            return handleError(res, err);
        }
        if (!Image) {
            return res.status(404).send('Not Found');
        }
        return res.json(image);
    });
};

// Executes search
exports.search = function (req, res) {
    Image.find().or([{
            name: req.query.name
        },
        {
            contact: req.query.contact
        }
    ]).exec(function (err, image) {
        if (err) {
            return handleError(res, err);
        }
        if (!image) {
            return res.status(404).send('Not Found');
        }
        return res.json(image);
    })
};
// query.or([{ color: 'red' }, { status: 'emergency' }])

// Creates a new image in the DB.
exports.create = function (req, res) {
    // Image.create({
    //     user_id: req.body.user_id,
    //     url: req.body.url
    // }
    Image.create(req.body, function (err, image) {
        console.info("new image body " + JSON.stringify(req.body));

        if (err) {
            return handleError(res, err);
        }
        return res.status(201).json(image);
    });
};

// Updates an existing image in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Image.findById(req.params.id, function (err, image) {
        if (err) {
            return handleError(res, err);
        }
        if (!image) {
            return res.status(404).send('Not Found');
        }
        var updated = _.merge(Image, req.body);

        updated.save(function (err) {
            if (err) {
                return handleError(res, err);
            }

            return res.status(200).json(image);
        });
    });
};

// Deletes an image from the DB.
exports.destroy = function (req, res) {
    Image.findById(req.params.id, function (err, image) {
        if (err) {
            return handleError(res, err);
        }
        if (!image) {
            return res.status(404).send('Not Found');
        }
        Image.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }
            return res.status(204).send('No Content');
        });
    });
};

function handleError(res, err) {
    console.log(">> " + err)
    return res.status(500).send(err);
}