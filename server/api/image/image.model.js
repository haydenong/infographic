'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ImageSchema = new Schema({
        user_id: String,
        url: String,
        created_at: Date,
        updated_at: Date
    });

module.exports = mongoose.model('Image', ImageSchema);