var gulp = require("gulp"),
    sass = require("gulp-ruby-sass"),
    concat = require('gulp-concat'),
    uglify = require("gulp-uglify"),
    series = require('stream-series'),
    inject = require("gulp-inject"),
    angularFilesort = require("gulp-angular-filesort"),
    nodemon = require("gulp-nodemon"),
    browserSync = require("browser-sync").create(),
    mainBowerFiles = require("main-bower-files"),
    bowerNormalizer = require("gulp-bower-normalize"),
    cleanCSS = require('gulp-clean-css'),
    order = require('gulp-order');

gulp.task('default', ["browser-sync"], function () {});

gulp.task('browser-sync', ['nodemon'], function () {
    browserSync.init(null, {
        proxy: "http://localhost:8080",
        files: ["client/**/*.*"],
        browser: "chrome",
        port: 7000
    });
});

gulp.task('nodemon', ["index"], function (cb) {
    var started = false;
    return nodemon({
        script: 'server/app.js'
    }).on('start', function () {
        // to avoid nodemon being started multiple times
        if (!started) {
            cb();
            started = true;
        }
    });
});

gulp.task('index', ["sass"], function () {
    var vendorStream = gulp.src(['dep.min.js'], {
        read: false,
        cwd: __dirname + '/client'
    });

    var appStream = gulp.src(['./app/**/*.js'], {
            read: true,
            cwd: __dirname + '/client'
        })
        .pipe(angularFilesort());

    gulp.src('./client/index.html')
        .pipe(inject(
            gulp.src("./assets/stylesheets/**/*.css", {
                read: false,
                cwd: __dirname + '/client'
            })
        ))
        .pipe(inject(series(vendorStream, appStream)))
        .pipe(gulp.dest('./client'));
});

gulp.task("sass", ["concat-css"], function () {
    sass("./client/assets/stylesheets/scss/index.scss")
        .on("error", sass.logError)
        .pipe(gulp.dest("./client/assets/stylesheets/css"));
});

gulp.task("concat-css", ["concat-js"], function () {
    gulp.src("./dependencies/css/**/*.css")
        .pipe(order(["bootstrap.css", "**/*.css"]))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(concat('dep.min.css'))
        .pipe(gulp.dest("./client/assets/stylesheets/css"));
})

gulp.task("concat-js", ["bower"], function () {
    gulp.src("./dependencies/js/**/*.js")
        .pipe(order(["jquery.js", "bootstrap.js", "angular.js","**/*.js"]))
        .pipe(uglify())
        .pipe(concat('dep.min.js'))
        .pipe(gulp.dest("./client"));
})

gulp.task("bower", function () {
    gulp.src(mainBowerFiles({
            paths: {
                bowerJson: 'bower.json',
                bowerDirectory: 'bower_components'
            }
        }))
        .pipe(bowerNormalizer({
            bowerJson: './bower.json'
        }))
        .pipe(gulp.dest('./dependencies'));
});